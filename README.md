# Wholesale Customers

**We create an unsupervised learning model to group the clients of a
wholesale distributor based on their annual spending on different
products.**

## Data

Source: https://archive.ics.uci.edu/ml/datasets/Wholesale+customers

The data set refers to clients of a wholesale distributor. It includes the annual spending in monetary units (m.u.) on 6 product categories.


Attribute Information:

* Fresh: annual spending (m.u.) on fresh products (Continuous)
* Milk: annual spending (m.u.) on milk products (Continuous)
* Grocery: annual spending (m.u.) on grocery products (Continuous)
* Frozen: annual spending (m.u.) on frozen products (Continuous)
* Detergents_Paper: annual spending (m.u.) on detergents and paper products (Continuous)
* Delicatessen: annual spending (m.u.) on and delicatessen products (Continuous)
* Channel: customers  Channel - Horeca (Hotel/Restaurant/Cafe) or Retail channel (Nominal)
* Region: customers  Region Lisnon, Oporto or Other (Nominal)
